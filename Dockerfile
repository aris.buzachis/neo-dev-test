FROM alpine AS frontend

RUN mkdir /app
WORKDIR /app

COPY frontend /app/frontend

#FAIL

RUN ls /app/frontend/

RUN apk add --update --no-cache python3 py3-pip

RUN pip3 install --upgrade --no-cache-dir pip \
  && pip3 install --no-cache-dir flask


FROM alpine

# test12345678
COPY --from=frontend /app/frontend/yarn.lock /app/yarn.lock

CMD ["sleep", "999999"]
